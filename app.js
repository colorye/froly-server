import express from 'express'
import cors from 'cors'

import graphqlHTTP from 'express-graphql';
import schema from './server/graphql/schema'

import logger from 'morgan'
import bodyParser from 'body-parser'
import routes from './server/routes'

// Set up the express app
const app = express();
app.use('*', cors({ origin: 'http://localhost:4000' }));

// Log requests to the console.
app.use(logger('dev'));

// GraphqQL server route
app.use('/graphql', graphqlHTTP(req => ({
    schema,
    pretty: true
})));
app.use('/graphiql', graphqlHTTP(req => ({
    schema,
    graphiql: true,
    pretty: true
})));

// Parse incoming requests data (https://github.com/expressjs/body-parser)
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

routes(app);
app.get('*', (req, res) => res.status(200).send({
    message: 'Welcome to the beginning of nothingness.',
}));

export default app;